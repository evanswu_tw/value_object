import java.util.Objects;

public class Euro {

    private double amount;

    public Euro(double amount) {
        this.amount = amount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Euro euro = (Euro) o;
        return Double.compare(euro.amount, amount) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(amount);
    }

    public Euro add(Euro euro) {
        return new Euro(amount += euro.amount);
    }
}
