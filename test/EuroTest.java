import org.junit.Test;

import static org.junit.Assert.*;

public class EuroTest {

    // this test and the next test is the same
    // I assume this one has something to do with
    // assertNotSame instead?
    @Test
    public void shouldBeSameEuroWithAssertEquals(){
        assertEquals(new Euro(10.0), new Euro(10.0));
    }

    // Yes, you are right!
    // Actually I think this will tell us that they are not the same object,
    // so I just use assertNotSame to justify their being different object.
    // I think last test should be a mistaken.
    // Thanks~!
    @Test
    public void shouldBeNotSameObjectEuroWithAssertSame(){
        assertNotSame(new Euro(10.0), new Euro(10.0));
    }



    @Test
    public void shouldNotBeEqualsWithDifferntAmoutOfMoney(){
        assertNotEquals(new Euro(10.0), new Euro(5.0));
    }
    @Test
    public void shouldNotBeEqualsWithANullValue(){
        assertNotEquals(new Euro(10.0), null);
    }

    @Test
    public void shouldNotBeEqualsWithAGenericObject(){

        assertNotEquals(new Euro(10.0),new Object());
    }

    @Test
    public void shouldBeEqualWithAddFunctionWithValue3Plus7(){

        assertEquals(new Euro(10.0),new Euro(3.0).add( new Euro(7.0)));
    }

    @Test
    public void shouldNotBeEqualWithAddFunctionWithValue5Plus2(){

        assertNotEquals(new Euro(10.0),new Euro(5.0).add( new Euro(2.0)));
    }


}
